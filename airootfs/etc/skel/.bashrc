# Add nano as default editor
export EDITOR=nano
export TERMINAL=lxterminal
export BROWSER=firefox

# Add scripts path
export PATH=$PATH:~/Scripts

# Gtk themes 
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

alias ls='ls --color=auto'

# Package sizes
alias pkg_size="expac -H M '%m\t%n' | sort -h"

# pacman scripts
alias setup-mirror='sudo l3afpad /etc/pacman.d/mirrorlist'

